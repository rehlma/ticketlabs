package de.reftec.ticketlabs.Main;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import de.reftec.ticketlabs.Types.Event;
import de.reftec.ticketlabs.Types.Ticket;
import de.reftec.ticketlabs.Utils.DBController;
import de.reftec.ticketlabs.Utils.Hashing;
import de.reftec.ticketlabs.Utils.JSONutil;
import de.reftec.ticketlabs.Utils.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RestAPI extends JSONutil{

	public static final int UPDATE_ERROR = 0;
	public static final int UPDATE_OK = 1;
	public static final int HTTP_ERROR = 4;
	public static final int LOGIN_OK = 5;
	public static final int LOGIN_FAILD = 6;
	
	private String user;
	private String pin;

	public RestAPI() {
		setReference(this);
	}
	
	public int login(String user, String pin) {
		if(checkLogin(user, pin)) {
			this.user = user;
			this.pin = pin;
			return LOGIN_OK;
		}
		return LOGIN_FAILD;
	}
	
	public String getHeader() throws NoSuchAlgorithmException {
		return getHeader(this.user, this.pin);
	}
	
	public String getHeader(String user, String pin) throws NoSuchAlgorithmException {
//		Datum: 31.03.2013 20 Uhr
//		Benutzer: maikel
//		Pin: Sicherheits123Pin
//		Aufbau des tokens: 2013maikel03Sicherheits123Pin31-20
//		date('Y').$dataItems['username'].date('m').$dataItems['pin'].date('d-H')
		
		Date date = new Date();
		DateFormat dateToken_1 = new SimpleDateFormat("yyyy");
		DateFormat dateToken_2 = new SimpleDateFormat("MM");
		DateFormat dateToken_3 = new SimpleDateFormat("dd-HH");
		System.out.println("Token: " + dateToken_1.format(date) + user + dateToken_2.format(date) + pin + dateToken_3.format(date));
		return Hashing.returnSHA512(dateToken_1.format(date) + user + dateToken_2.format(date) + pin + dateToken_3.format(date));
	}
	
	public boolean checkLogin(String user, String pin) {
		boolean loggedin = false;
		try {
			URL url = new URL("http://tickets.impresso-server.de/users/isloggedin.json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("X-ReftecApiToken", getHeader(user, pin));
            System.out.println("Return: Header: " + getHeader(user, pin));
			
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			
			String output;
			while ((output = br.readLine()) != null) {
				System.out.println("Return: CheckedLogin: " + output);
				JsonObject root = JsonObject.readFrom(output);
				JsonObject element = root.get("ContainerElement").asObject();
				loggedin = element.get("User").asObject().get("isloggedin").asBoolean();
			}
			conn.disconnect();
		}
		catch (Exception e) {
			Log.wirteLog("checkLogin", e.getMessage());
			e.printStackTrace();
		}
		return loggedin;
	}
	
	// Events
	public void getEvents() {
		try {
			URL url = new URL("http://tickets.impresso-server.de/events/index.json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("X-ReftecApiToken", getHeader());
			
			OutputStream os = conn.getOutputStream();
			os.write(getResultString().getBytes());
			os.flush();
			
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				JsonObject root = JsonObject.readFrom(output);
				JsonArray array = root.get("ContainerCollection").asArray();

				for (int i = 0; i < array.size(); i++) {
					JsonObject element = array.get(i).asObject();

					JsonObject mEvent = element.get("Event").asObject();
					Event event = new Event();
					event.setId(mEvent.get("id").asInt());
					event.setTitle(mEvent.get("title").asString());
					event.setDate(mEvent.get("begin").asString());
					DBController.getInstance().putEvent(event);
				}
			}
			conn.disconnect();
		}
		catch (Exception e) {
			Log.wirteLog("getEvents", e.getMessage());
			e.printStackTrace();
		}
		finally { clearFilter(); }
	}
	
	// Tickets
	public void getTickets() {
		try {
			URL url = new URL("http://tickets.impresso-server.de/tickets/index.json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("X-ReftecApiToken", getHeader());
			
			OutputStream os = conn.getOutputStream();
			os.write(getResultString().getBytes());
			os.flush();
			
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			
			String output;
			while ((output = br.readLine()) != null) {
				JsonObject root = JsonObject.readFrom(output);
				JsonArray array = root.get("ContainerCollection").asArray();
				
				for (int i = 0; i < array.size(); i++) {
					JsonObject element = array.get(i).asObject();
					
					JsonObject mTicket = element.get("Ticket").asObject();
					Ticket ticket = new Ticket();
					ticket.setArchived(mTicket.get("archived").asBoolean());
					ticket.setValue(mTicket.get("value").asDouble());
					ticket.setId(mTicket.get("id").asInt());
					ticket.setEventId(Integer.valueOf(mTicket.get("event_id").asString()));
					ticket.setUsed(mTicket.get("used").asString());
					ticket.setIsUsed(mTicket.get("is_used").asBoolean());
					DBController.getInstance().putTicket(ticket);
				}
			}
			conn.disconnect();
		}
		catch (Exception e) {
			Log.wirteLog("getTickets", e.getMessage());
			e.printStackTrace();
		}
		finally { clearFilter(); }
	}

	// update Ticket
	public int updateTicket(Ticket ticket) {
		try {
			//Get ID from tickets;
			int id = ticket.getId();
			URL url = new URL("http://tickets.impresso-server.de/tickets/edit/"	+ id + ".json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Content-Type", "Application/json");
			conn.setRequestProperty("Accept", "Application/json");
			conn.setRequestProperty("X-ReftecApiToken", getHeader());

			String input = "{\"Ticket\":{\"id\":" + id + ",\"used\":\"" + ticket.getDate() + " " + ticket.getTime() + "\",\"is_used\":" + ticket.getIsUsed() + "}}";
			
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return HTTP_ERROR;
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
			return UPDATE_OK;
		}
		catch (Exception e) {
			Log.wirteLog("updateTicket", e.getMessage());
			e.printStackTrace();
		}
		return UPDATE_ERROR;
	}
}
