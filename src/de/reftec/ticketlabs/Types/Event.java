package de.reftec.ticketlabs.Types;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Event {
	
	private SimpleIntegerProperty id;
	private SimpleStringProperty date;
	private SimpleStringProperty time;
	private SimpleStringProperty title;
	
	public Event() {
		this.id = new SimpleIntegerProperty();
		this.date = new SimpleStringProperty();
		this.time = new SimpleStringProperty();
		this.title = new SimpleStringProperty();
	}
	
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);
	}	
	public String getDate() {
		return date.get();
	}
	public String getTime() {
		return time.get();
	}
	public void setDate(String date) {
		this.date.set(date.substring(0,10));
		this.time.set(date.substring(11, 19));
	}		
	public String getTitle() {
		return title.get();
	}
	public void setTitle(String title) {
		this.title.set(title);
	}
	
	@Override
	public String toString() {
		return title.get();
	}
}
