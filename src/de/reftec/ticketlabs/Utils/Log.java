package de.reftec.ticketlabs.Utils;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {
	public static void wirteLog (String Tag, String Text) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(Calendar.getInstance().getTime());
		
	    File logFile = new File("TicketSystem.log");
        try {
        	FileWriter writer = new FileWriter(logFile ,true);
        	writer.write(System.getProperty("line.separator"));
        	writer.write("Timestamp: " + timeStamp + "\tTag: " + Tag + "\tMessage: " + Text);
        	writer.write(System.getProperty("line.separator"));
        	
        	writer.flush();
            writer.close();
        }
        catch (Exception e) {
        	System.out.println("Failed to create LogFile");
        }
	}
}
