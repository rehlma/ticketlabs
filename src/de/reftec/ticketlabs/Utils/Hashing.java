package de.reftec.ticketlabs.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashing {
	/**
	 * Create a SHA512 value of a string
	 * @param string
	 * @return SHA512 Hash
	 * @throws NoSuchAlgorithmException
	 */
	public static String returnSHA512(String string) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		md.update(string.getBytes());
		byte byteData[] = md.digest();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
					.substring(1));
		}

		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}
}
